package aplicacao;

import java.util.*;


public class App {
	
	protected GerenciarCliente gerenCliente = new GerenciarCliente();
	protected GerenciarConta gerenConta = new GerenciarConta();
	
	public static Scanner scan = new Scanner(System.in);
	private int userOption = 0;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		App app = new App();
		
		//  MAIN MENU 
		while(app.userOption != 3) {
			System.out.println("===== SISTEMA BANCÁRIO =====");
			Menus.mainMenu();
			System.out.println("Digite a tecla correspondente a opção desejada: ");
			app.userOption = scan.nextInt();
			
			// GERENCIAR CLIENTE
			if (app.userOption == 1) {
				Utils.clearScreen();
				while(app.userOption != 5) {
					Utils.clearScreen();
					System.out.println("\nGerenciamento de Clientes");
	 				Menus.menuGerenciarCliente();
					System.out.println("Digite a tecla correspondente a opção desejada: ");
					
					app.userOption = App.scan.nextInt();
		
					if (app.userOption == 1) {	 
						Cliente cliente = app.gerenCliente.formCriarCliente();
						
						if (cliente != null && app.gerenCliente.consultar(cliente.nome) != null) {
							System.out.println("Cliente já existe!!! Favor criar outro cliente!");
						} else {
							if (app.gerenCliente.criar(cliente)) {
								System.out.println("Sucesso ao criar cliente! \n\nCódigo: " + cliente.getCodigo() + "\nNome: " + cliente.nome + "\n");
							} else {
								System.out.println("Não foi possível criar o cliente... tente novamente mais tarde!");
							}
						}
						
						System.out.println("Digite qualquer tecla para continuar\n");
					} 
					// Consultar cliente
					else if (app.userOption == 2) {
						
						Cliente cliente = null;
						
						System.out.println("Digite o código ou o nome do cliente que deseja pesquisar: ");
						String query = scan.nextLine();
						query = scan.nextLine();
						
						if (Utils.stringIsNumeric(query)) {
							cliente = app.gerenCliente.consultar(Integer.parseInt(query));
						} else {
							cliente = app.gerenCliente.consultar(query);
						}
						
						if (cliente != null) {
							System.out.println("Cliente encontrado!!!");
							System.out.println("\n\nCódigo: " + cliente.getCodigo() + "\nNome: " + cliente.nome + "\n\n");
						} else {
							System.out.println("Não foi possível encontrar o cliente... verifique se está digitando o código corretamente!");
						}
					} 
					// Remover
					else if (app.userOption == 3) {
						Cliente cliente = null;
						
						System.out.println("Digite o código ou o nome do cliente que deseja pesquisar: ");
						String query = scan.nextLine();
						query = scan.nextLine();
						
						if (Utils.stringIsNumeric(query)) {
							cliente = app.gerenCliente.consultar(Integer.parseInt(query));
						} else {
							cliente = app.gerenCliente.consultar(query);
						}
						
						if (cliente != null) {
							app.gerenCliente.remover(cliente.getCodigo());
							System.out.println("Cliente deletado com sucesso!!!");
						} else {
							System.out.println("Não foi possível encontrar o cliente... verifique se está digitando o código corretamente!");
						}
					}
					
					// Atualizar
					else if (app.userOption == 4) {
						Cliente cliente = null;
						
						System.out.println("Digite o código ou o nome do cliente que deseja pesquisar: ");
						String query = scan.nextLine();
						query = scan.nextLine();
						
						if (Utils.stringIsNumeric(query)) {
							cliente = app.gerenCliente.consultar(Integer.parseInt(query));
						} else {
							cliente = app.gerenCliente.consultar(query);
						}
						
						if (cliente != null) {
							System.out.println("Cliente a ser atualizado: \nCódigo: " + cliente.getCodigo() + "\nNome: " + cliente.nome);
							if (app.gerenCliente.atualizar(cliente.getCodigo(), app.gerenCliente.formAtualizarCliente(cliente))) {
								System.out.println("Cliente atualizado com sucesso!!!");
							} else {
								System.out.println("Não foi possível atualizar o cliente");
							}
						}
					} 
					// Voltar
					else if (app.userOption == 5) {
						Utils.clearScreen();
					} 
					else {
						System.out.println("\nOpção digitada incorretamente! Tente novamente com as opções oferecidas!");
					}
				};
			} 
			//////////////////////////////////////////
			//// GERENCIAR CONTA		
			//////////////////////////////////////////
			else if (app.userOption == 2) {
				
				while(app.userOption != 6) {
					Utils.clearScreen();
					System.out.println("\nGerenciamento de Conta");
	 				Menus.menuGerenciarConta();
					System.out.println("Digite a tecla correspondente a opção desejada: ");
					
					app.userOption = App.scan.nextInt();
		
					// Criar conta para cliente
					if (app.userOption == 1) {	
						Cliente cliente = null;
						System.out.println("=> Criar conta cliente");
						
						System.out.println("Digite o código ou o nome do cliente que deseja pesquisar: ");
						String query = scan.nextLine();
						query = scan.nextLine();
						
						if (Utils.stringIsNumeric(query)) {
							cliente = app.gerenCliente.consultar(Integer.parseInt(query));
						} else {
							cliente = app.gerenCliente.consultar(query);
						}
						
						if (cliente != null) {
							Conta contaCliente = new Conta(cliente);
							
							if (contaCliente != null) {
								app.gerenConta.criar(contaCliente);
								System.out.println("Sucesso ao criar uma conta para cliente! \n\nCódigo: " + contaCliente.getCodigo() + "\nTítular: " + contaCliente.getDono().nome + "\nSaldo: R$" + contaCliente.saldo);
							}
						} else {
							System.out.println("Não foi possível encontrar o cliente!");
						}
						System.out.println("Digite qualquer tecla para continuar\n");
					// Sacar dinheiro 
					} else if (app.userOption == 2) {
						System.out.println("Digite o código da conta ao qual deseja depositar: ");
						int codigoPesq = scan.nextInt();
						
						
						Conta contaCliente = app.gerenConta.consultar(codigoPesq);
						
						if (contaCliente != null) {
							System.out.println("Digite o valor que deseja depositar");
							double valor = scan.nextDouble();
							
							if (valor <= contaCliente.saldo) {
								contaCliente.saldo -= valor;
								app.gerenConta.atualizar(contaCliente.getCodigo(), contaCliente);
								System.out.println("Saque realizado com sucesso com sucesso!!!");

								System.out.println("Saldo da conta: R$" + contaCliente.saldo);
							} else {
								System.out.println("Conta não possui saldo suficiente para operação!!!");
								System.out.println("Saldo da conta: R$" + contaCliente.saldo + "\n");
							}
						} else {
							System.out.println("Não foi possível encontrar a conta!!!");
						}
					} 
					// Depositar dinheiro 
					else if (app.userOption == 3) {
						System.out.println("Digite o código da conta ao qual deseja depositar: ");
						int codigoPesq = scan.nextInt();
						Conta contaCliente = app.gerenConta.consultar(codigoPesq);
						
						if (contaCliente != null) {
							System.out.println("Digite o valor que deseja depositar");
							double valor = scan.nextDouble();
							
							if (valor >= 0) {
								contaCliente.saldo += valor;
								app.gerenConta.atualizar(contaCliente.getCodigo(), contaCliente);
								System.out.println("Saldo atualizado com sucesso!!! Saldo atual: R$" + contaCliente.saldo+ "\n");
							}
							System.out.println("Saldo da conta: R$" + contaCliente.saldo+ "\n");
						} else {
							System.out.println("Não foi possível encontrar a conta!!!"+ "\n");
						}
					}
					
					// Verificar saldo
					else if (app.userOption == 4) {
						System.out.println("Digite o código da conta ao qual deseja pesquisar: ");
						int codigoPesq = scan.nextInt();
						Conta contaCliente = app.gerenConta.consultar(codigoPesq);
						
						if (contaCliente != null) {
							System.out.println("Saldo da conta: R$" + contaCliente.saldo);
						} else {
							System.out.println("Não foi possível encontrar a conta!!!");
						}
					} 
					// Voltar
					else if (app.userOption == 5) {
						System.out.println("Digite o código da conta remetente: ");
						int codigoPesq = scan.nextInt();
						Conta contaRemet = app.gerenConta.consultar(codigoPesq);
						
						if (contaRemet != null) {
							System.out.println("Titular da conta: " + contaRemet.getDono().nome);
							System.out.println("Digite o código da conta destinatária:");
							codigoPesq = scan.nextInt();
							Conta contaDest = app.gerenConta.consultar(codigoPesq);
							
							if (contaDest != null) {
								System.out.println("Titular da conta: " + contaDest.getDono().nome);
								System.out.println("Digite o valor que deseja transferir");
								double valor = scan.nextDouble();
								
								if (valor <= contaRemet.saldo) {
									contaDest.saldo += valor;
									contaRemet.saldo -= valor;
									app.gerenConta.atualizar(contaRemet.getCodigo(), contaRemet);
									app.gerenConta.atualizar(contaDest.getCodigo(), contaDest);
									System.out.println("Transferencia de atualizado com sucesso!!!");
									System.out.println("Saldo da conta remetente: R$" + contaRemet.saldo+ "\n");
								} else {
									System.out.println("Conta remetente não possui saldo suficiente para operação!!!");
									System.out.println("Saldo da conta: R$" + contaRemet.saldo + "\n");
								}
							} else {
								System.out.println("Não foi possível encontrar a conta destinatária!!!"+ "\n");
							}
							
						} else {
							System.out.println("Não foi possível encontrar a conta remetente!!!"+ "\n");
						}
					} else if (app.userOption == 6) { 
						Utils.clearScreen();
					} else {
						System.out.println("\nOpção digitada incorreta, tente novamente com as opçoes oferecidas!");
					}
				};
			} 
			// SAIR DO SISTEMA
			else if (app.userOption == 3) {
				System.out.println("Obrigado por utilizar nosso sistema");
				System.exit(0);
			} else {
				System.out.println("Digite a tecla correspondente a opção desejada: ");
			}					
		
	}
}
}
