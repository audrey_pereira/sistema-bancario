package aplicacao;

import java.util.Random;

public class Cliente {

	private int codigo;
	protected String nome;
	
	protected int getCodigo() {
		return this.codigo;
	}
	
	
	public Cliente(String _nome) {
		this.codigo = this.gerarCodigoConta();
		this.nome = _nome;
	}
	
	private int gerarCodigoConta() {
		String codigo = "0000";
		Random rand = new Random();
	
		codigo += rand.nextInt(10000);
		return Integer.parseInt(codigo);
	}
}
