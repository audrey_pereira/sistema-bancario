package aplicacao;

public class Menus {
	
	public static void mainMenu() {
		System.out.println("1 - Gerenciar CLIENTES\n2 - Gerenciar CONTAS\n3 - Sair\n");
	}
	
	public static void menuGerenciarCliente() {
		System.out.println("1-Cadastrar CLIENTE\n" + 
				"2-Consultar CLIENTE\n" + 
				"3-Remover CLIENTE\n" + 
				"4-Atualizar CLIENTE\n" + 
				"5-Voltar ao MENU INICIAL\n");
	}
	
	public static void menuGerenciarConta() {
		System.out.println("1-Criar CONTA para um CLIENTE\n" + 
				"	2-Sacar dinheiro de uma CONTA de um CLIENTE\n" + 
				"	3-Depositar dinheiro para uma CONTA de um CLIENTE\n" + 
				"	4-Verificar saldo de uma CONTA de um CLIENTE\n" + 
				"	5-Transferir dinheiro de uma CONTA de um CLIENTE para outro CLIENTE\n" + 
				"	6-Voltar ao MENU INICIAL\n");
	}
	
	
}
