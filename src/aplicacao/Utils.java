package aplicacao;

public class Utils {
	
	public static void clearScreen(){
	   try {
		    if (System.getProperty("os.name").contains("Windows"))
	            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
	        else
	            Runtime.getRuntime().exec("clear");
        } catch(final Exception e) {
            System.out.print(e);
        };  
	}
	
	public static boolean stringIsNumeric(String search) {
		try {
			int codigo = Integer.parseInt(search);
			return true;
		} catch(Exception ex) {
			return false;
		}
	}

}
