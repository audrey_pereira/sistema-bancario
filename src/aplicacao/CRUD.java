package aplicacao;

public abstract class CRUD {
	protected abstract boolean criar(Object obj);
	protected abstract boolean atualizar(String identificador, Object objAtualizado);
	protected abstract boolean remover(String identificiador);
	protected abstract Object consultar(String identificador);
}