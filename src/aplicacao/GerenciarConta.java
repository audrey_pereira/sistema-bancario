package aplicacao;

import java.util.ArrayList;

public class GerenciarConta extends CRUD {
	
	private ArrayList<Conta> repositoryConta = new ArrayList<Conta>();
	
	protected boolean criar(Conta conta) {
		for (Conta ct : repositoryConta) {
			if (ct.getCodigo() == conta.getCodigo()) {
				return false;
			}
		}	
			
		repositoryConta.add(conta);
		return true;
	}
	
	protected boolean atualizar(int identificador, Conta contaAtualizada) {
		for (Conta conta : repositoryConta) {
			if (conta.getCodigo() == identificador) {
				repositoryConta.set(repositoryConta.indexOf(conta), contaAtualizada);
				return true;
			}
		}
		
		return false;
	} 
	
	protected boolean atualizar(String nomeCompleto, Conta contaAtualizada) {
		for (Conta conta : repositoryConta) {
			if (conta.getDono().nome.equals(nomeCompleto)) {
				repositoryConta.set(repositoryConta.indexOf(conta), contaAtualizada);
				return true;
			}
		}
		
		return false;
	} 
	
	protected boolean remover(int identificiador) {
		for (Conta conta : repositoryConta) {
			if (conta.getCodigo() == identificiador) {
				repositoryConta.remove(conta);
				return true;
			}
		}
	
		return false;
	}
	protected Conta consultar(int identificador) {
		for (Conta conta : repositoryConta) {
			if (conta.getCodigo() == identificador) {
				return conta;
			}
		}
		
		return null;
	}

	protected void mostrarContas() {
		for (Conta conta : this.repositoryConta) {
			System.out.println("\nCódigo: " + conta.getCodigo() + "\nTítular: " + conta.getDono().nome + "\nSaldo: R$" + conta.saldo);
		}
	}

	@Override
	protected boolean criar(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean atualizar(String identificador, Object objAtualizado) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean remover(String identificiador) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected Object consultar(String identificador) {
		// TODO Auto-generated method stub
		return null;
	}
}
