package aplicacao;

import java.util.ArrayList;

public class GerenciarCliente extends CRUD {
	
	private ArrayList<Cliente> repositoryCliente = new ArrayList<Cliente>();
	
	protected Cliente formCriarCliente() {
		String nomeCliente = "";
		Cliente cliente = null;
		
		System.out.println("Digite o nome completo do cliente: \n");
		nomeCliente = App.scan.nextLine();
		nomeCliente = App.scan.nextLine(); // Bugzinho java scanner.nextLine()
		
		if (nomeCliente == "") {
			return null;
		}
		
		cliente = new Cliente(nomeCliente);
		return cliente;
	}
	
	protected boolean criar(Cliente cliente) {
		if (repositoryCliente.add(cliente)) 
			return true;
		return false;
	}
	
	protected Cliente formAtualizarCliente(Cliente cliente) {	
		System.out.println("Digite o nome completo do cliente: \n");
		cliente.nome = App.scan.nextLine();
		cliente.nome = App.scan.nextLine();
		
		if (cliente.nome == "") {
			return null;
		}
		
		return cliente;
	}
	
	protected boolean atualizar(int identificador, Cliente clienteAtualizado) {
		for (Cliente cliente : repositoryCliente) {
			if (cliente.getCodigo() == identificador) {
				repositoryCliente.set(repositoryCliente.indexOf(cliente), clienteAtualizado);
				return true;
			}
		}
		
		return false;
	} 
	
	protected boolean atualizar(String nomeCompleto, Cliente clienteAtualizado) {
		for (Cliente cliente : repositoryCliente) {
			if (cliente.nome.equals(nomeCompleto)) {
				repositoryCliente.set(repositoryCliente.indexOf(cliente), clienteAtualizado);
				return true;
			}
		}
		
		return false;
	} 
	
	protected boolean remover(String nomeCompleto) {
		for (Cliente cliente : repositoryCliente) {
			if (cliente.nome.equals(nomeCompleto)) {
				repositoryCliente.remove(cliente);
				return true;
			}
		}
	
		return false;
	}
	
	protected boolean remover(int identificador) {
		for (Cliente cliente : repositoryCliente) {
			if (cliente.getCodigo() == identificador) {
				repositoryCliente.remove(cliente);
				return true;
			}
		}
	
		return false;
	}
	
	protected Cliente consultar(String nomeCompleto) {
		for (Cliente cliente : repositoryCliente) {
			if (cliente.nome.equals(nomeCompleto)) {
				return cliente;
			}
		}
		
		return null;
	}
	
	protected Cliente consultar(int identificador) {
		for (Cliente cliente : repositoryCliente) {
			if (cliente.getCodigo() == identificador) {
				return cliente;
			}
		}
		
		return null;
	}
	
	protected void mostrarClientes() {
		for (Cliente cliente : repositoryCliente) {
			System.out.println("\n\nCódigo: " + cliente.getCodigo() + "\nNome: " + cliente.nome + "\n");
		}
	}
	
	

	@Override
	protected boolean criar(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean atualizar(String identificador, Object objAtualizado) {
		// TODO Auto-generated method stub
		return false;
	}
}
