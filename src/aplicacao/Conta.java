package aplicacao;

import java.util.Random;

public class Conta {
	private int codigo;
	protected double saldo = 0;
	private Cliente dono;
	
	protected int getCodigo() {
		return this.codigo;
	}
	
	protected Cliente getDono() {
		return this.dono;
	}
	
	public Conta(Cliente _donoConta) {
		this.codigo = this.gerarCodigoConta();
		this.dono = _donoConta;
	}
	
	private int gerarCodigoConta() {
		String codigo = "0000";
		Random rand = new Random();
	
		codigo += rand.nextInt(10000);
		return Integer.parseInt(codigo);
	}
}
